console.log("Hello World");

let firstName = "John";
let lastName = "Smith";
let age = 30;
let hobbies = ['Biking','Mountain Climbing','Swimming'];
let address = {
	houseNumber: '32',
	street: 'Washington',
	city: 'Lincoln',
	state: 'Nebraska',
};

console.log(`First Name: ${firstName}`);
console.log(`Last Name: ${lastName}`);
console.log(`Age: ${age}`);
console.log(`Hobbies:`);
console.log(hobbies);
console.log(`Work Address:`);
console.log(address);



function printUserInfo(firstName, lastName, age) {
	console.log(`
	${firstName} ${lastName} is ${age} years of age.
	`);
	console.log(`This was printed inside printUserInfo function:`);
	console.log(`Hobbies:`);
	console.log(hobbies);
	console.log(`This was printed inside printUserInfo function:`);
	console.log(`Work Address:`);
	console.log(address);
}

printUserInfo("John", "Smith", 30);

function returnValue(value) {
	return value;
}

let isMarried = returnValue(true);

console.log(`The value of isMarried is: ${isMarried}`);

